# prompting_adversarial_stylometry

Learning disentanglement representation with ByT5-based VAE to conceal privacy-sensitive information found in one's writing style.